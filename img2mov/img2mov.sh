#!/bin/sh

###############################################################################
# FUNCTIONS
###############################################################################
usage()
{
	echo "NAME"
	echo "        img2mov - Convert a series of images to a mp4 video"
	echo
	
	echo "DEPENDENCIES"
	echo "        ffmpeg"
	echo
	
	echo "SYNOPSIS"
	echo "        ./img2mov.sh [-d <path>|-t <pattern>|-f <extension>|-o <filename>|-y|-r]"
	echo
	
	echo "DESCRIPTION"
	echo "        Search for images in <path>; and make a mp4 video with images corresponding to <pattern> and <extension>.  Video <filename> is saved in <path>"
	echo
	
	echo "OPTIONS"
	echo "        -d, --dir, --directory <path>"
	echo "            Set working directory where to get input images and where to save the output video file"
	echo "            When ommited, working directory is used"
	echo "        -t, --format <pattern>"
	echo "            Set input images filename format"
	echo "            Use ffmpeg filename pattern.  Do not specify the files extension here (See option -f)."
	echo "            When ommited, default pattern is \"*\""
	echo "            When used with option \"-r\", use only \"*\"; do not use \"%\""
	echo "        -f, --filetype <extension>"
	echo "            Set input images extension"
	echo "            When ommited, default extension is \"png\""
	echo "        -o, --output <filename>"
	echo "            Set output video filename.  Do not specify extension; it is always \".mp4\""
	echo "        -y, --yes"
	echo "            Force overwritting output video file"
	echo "        -r, --remove, --rm"
	echo "            Force remove input images after process"
	echo
	
	echo "EXAMPLES"
	echo "        Process any png images in the working directory"
	echo "            ./img2mov.sh -o output"
	echo "            Will generate video filename output.mp4 from all png file in the current directory"
	echo "        Specify input images filename and extension"
	echo "            ./img2mov.sh -d ~/CCTV/ -t MDAlarm_20180613-* -f jpg -o output"
	echo "            Will process all files with files names corresponding to \"MDAlarm_20180313-*.jpg\""
	echo "            Output video file will be stored as \"~/CCTV/output.mp4\""
	echo

	echo "KNOWN ISSUES"
	echo "        -r  Can only process filenames with shell wildcard"
	echo

	echo "FURTHER INFO"
	echo "        ffmpeg filename patterns"
	echo "            https://ffmpeg.org/ffmpeg-formats.html#image2-1"
	echo "            https://en.wikibooks.org/wiki/FFMPEG_An_Intermediate_Guide/image_sequence"
	echo
}

###############################################################################
#
# SCRIPT ENTRY POINT
#
###############################################################################
revertdate=""
inputdirectory="."		# Directory storing images to be processed
filenameformat="*"		# Input filename format
imagestype="png"		# Image extension to be processed
outputfilename="output"		# Video filename (extension is .mp4)
overwrite=false			# When set, overwrite existing video files
removeimages=false		# When set, remove image files after process

# # # # # #
# CHECK SCRIPT'S ARGUMENTS
#

# Get arguments from command line
while [ "$1" != "" ]; do
	case $1 in
		-d|--dir|--directory)	# Process images from given directory
			inputdirectory="$2"
			if [ "$inputdirectory" = "" ]; then
				echo "Error: Missing argument with option -d, --dir, --directory" ; echo
				usage | less
				exit 1
			fi
			shift 2
			;;
		-t|--format)	# Process images of the given filename format
			filenameformat="$2"
			if [ "$filenameformat" = "" ]; then
				echo "Error: Missing argument with option -f, --format" ; echo
				usage | less
				exit 1
			fi
			shift 2
			;;
		-f|--filetype)	# Process images of given filetype
			imagestype="$2"
			if [ "$imagestype" = "" ]; then
				echo "Error: Missing argument with option -f, --filetype" ; echo
				usage | less
				exit 1
			fi
			shift 2
			;;
		-o|--output)	# Output filename
			outputfilename="$2"
			if [ "$outputfilename" = "" ]; then
				echo "Error: Missing argument with option -o, --output" ; echo
				usage | less
				exit 1
			fi
			shift 2
			;;
		-y|--yes)       # Overwrite existing files without notice
			overwrite=true
			shift
			;;
		-r|--remove|--rm)	# Remove image files after process
			removeimages=true;
			shift
			;;
		*)
			usage | less
			exit 1
			;;
	esac
done

# # # # # #
# Convert images to video
#
if $overwrite; then
	ffmpeg -y -pattern_type glob -framerate 25 -i "${inputdirectory%/}/${filenameformat}.${imagestype}" -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "${inputdirectory%/}/${outputfilename}.mp4"
else
	ffmpeg -pattern_type glob -framerate 25 -i "${inputdirectory%/}/${filenameformat}.${imagestype}" -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "${inputdirectory%/}/${outputfilename}.mp4"
fi

# # # # # #
# Remove images after video conversion
#
if $removeimages; then
	rm "${inputdirectory%/}/*.${imagestype}"
fi
