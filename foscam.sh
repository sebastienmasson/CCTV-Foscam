#!/bin/sh

# # # # # #
# Build foscam images filename
#
IMG_PREFIX="MDAlarm_"
IMG_EXTENSION="jpg"
DATE=`date +%Y%m%d`

pattern="${IMG_PREFIX}${DATE}-*"

# # # # # #
# Build video filename
#
MOV_PREFIX="Parking"

videofilename="${MOV_PREFIX}-${DATE}"

# # # # # #
# Generat daily video file
#
img2mov.sh -d /media/telma/CCTV/snap/ -r -y -t "${pattern}" -f "${IMG_EXTENSION}" -o "${videofilename}"

# # # # # #
# Delete files older than a month
#
# Known limitations:
# - Sometimes tries to delete impossible date files.  Eg.: 30 Feb.
# - Last day of longer months requires two months before beeing deleted.  Eg.: 31 Aug. is going to be erased on 31 Oct.
MONTH=`date +%m`
echo "Month: $MONTH"
if [ "$MONTH" -eq 1 ]; then		# if we are in january
	YEAR=`date +%Y`
	YEAR=$(( YEAR-1 ))
	MONTH="01"
	DAY=`date +%d`
	DATE="${YEAR}${MONTH}${DAY}"
else					# if not
	DATE=$(( DATE-100 ))		# month = month - 1
fi

videofilename="${MOV_PREFIX}-${DATE}.mp4"
rm "$videofilename"			# Remove video archive
